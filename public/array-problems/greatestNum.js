const nums = [10, 30, 20, 50, 40, 70, 60, 90, 80, 100];

var largest = 0;
var greatestNum = null;

nums.forEach((num)=> {
  greatestNum = nums.reduce(()=> {
    if(num>largest) largest = num;
    else return num;
  })
})

console.log(`The greatest number from ${nums} is ${largest}`);
