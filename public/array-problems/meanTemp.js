const tempRecordings = [23, 27, 29, 32, 28, 26, 24];

const tempSum = tempRecordings.reduce((total, temp)=> total+temp, 0);

const meanTemp = tempSum/tempRecordings.length;

console.log(meanTemp);
