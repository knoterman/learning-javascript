var num = prompt('Enter a number');
if(num > 0){
    if(num % 2 === 0){
        console.log(`${num} is a even number`);
    } else{
        console.log(`${num} is an odd number`);
    }
}else{
    console.log('Please enter a positive integer');
}