//1. Generate a number between 1-100
//2. Prompt user to enter a number
//3. Compare the entered number and generated number are equal or not
//4. if the entered number is less than generated number, alert the generated number is greater than the guessed number
//5. if the entered number is greater than generated number, alert the generated number is less than guessed number
//6. Give 10 chances to the user

var num = Math.random() * 100;
num = Math.floor(num);
console.log(num);

for(let i = 0; i<10; i++){
    var user_input = parseInt(prompt('Guess a number between 1-100'));

    if(user_input == num){
        alert('You have guessed correctly');
        break;
    }

    if(num > user_input){
        alert(`${user_input} is less than the generated number`);
    }else{
        alert(`The generated number is less than ${user_input}`);
    }
}

alert('thank you for playing');