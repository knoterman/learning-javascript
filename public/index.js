const pi = Math.PI;
const input = document.getElementById('input');
const form = document.getElementById('form');
const submitBtn = document.getElementById('submitBtn');
const answer = document.getElementById('answer');

const list = document.querySelectorAll('li');
var func = null;

var functionName = null;

list.forEach((li)=> {
  li.addEventListener('click', ()=>{
    answer.innerText = '';
    input.value = '';
    input.placeholder = li.getAttribute('data-placeholder');
    functionName = li.getAttribute('data-function');
  })
})

function chooseFunction(n){
  switch(n){
    case 'basic1': areaAndCircumference();
                 break;
    case 'basic2': meanOfThreeNums();
                 break;
    case 'basic3': sumOfLastDigits();
                break;
    case 'basic4': volumneOfSphere();
                break;
    case 'basic5': sumOfThreeDigits();
                break;
    case 'ifElse1': canTriangle();
                break;
    case 'ifElse2': evenOdd();
                break;
    case 'ifElse3': hasPassed();
                break;
    case 'ifElse4': isEquilateral();
                break;
    case 'ifElse5': number_guessing_game();
                break;
    case 'loop1': OnetoNum();
                break;
    case 'loop2': allPrimeNum();
                break;
    case 'loop3': armstrong();
                break;
    case 'loop4': ascii();
                break;
    case 'loop5': commonFactors();
                break;
    case 'switch1': monthToWords();
                break;
    case 'switch2': numToWords();
                break;
    case 'switch3': operation_from_user_input();
                break;
    case 'switch4': totalVowels();
                break;
    case 'switch5': totalVowelsOfType();
                break;
    
  }
}

submitBtn.addEventListener('click', (e)=>{
  e.preventDefault();
  if(input.value !=="" || input.placeholder == 'Show answer') chooseFunction(functionName);
  else answer.innerText = 'Please enter a value';
})

function areaAndCircumference(){
  var r = input.value;
  var area = pi*r*r;
  var circumference = 2*pi*r;
  answer.innerText = `Area: ${area}\n Circumference: ${circumference}`;
}

function meanOfThreeNums(){
  var user_input = input.value.split(',');
  var n1 = parseInt(user_input[0]);
  var n2 = parseInt(user_input[1]);
  var n3 = parseInt(user_input[2]);
  var mean = (n1+n2+n3)/user_input.length;
  answer.innerText = mean;
}

function sumOfLastDigits(){
  var user_input = input.value.split(',');
  var n1 = user_input[0];
  var firstDigit = parseInt(n1[user_input[0].length - 1]);
  var n2 = user_input[1];
  var secondDigit = parseInt(n2[user_input[1].length - 1]);
  var sum = firstDigit + secondDigit;
  answer.innerText = sum;
}

function volumneOfSphere(){
  var radius = input.value;
  var volume = (4/3)*pi*radius*radius*radius;
  answer.innerText = `Volume of sphere with radius ${radius}: ${volume}`;
}

function sumOfThreeDigits(){
  var user_input = input.value;
  if(user_input.length !== 3) alert('Invalid input');
  else {
    var d1 = parseInt(user_input[0]);
    var d2 = parseInt(user_input[1]);
    var d3 = parseInt(user_input[2]);
    var sum = d1+d2+d3;
    answer.innerText = `Sum of ${d1}, ${d2} and ${d3} is ${sum}`;
  }
}

// IF-ELSE
function canTriangle(){
  const str = input.value.split(',');
  const a1 = parseInt(str[0]), a2 = parseInt(str[1]), a3 = parseInt(str[2]);

  if((a1+a2+a3) == 180){
    answer.innerText = 'triangle can be formed';
  }else{
    answer.innerText = 'triangle cannot be formed';
  }
}

function evenOdd(){
  var num = parseInt(input.value);
  if(num > 0){
      if(num % 2 === 0){
          answer.innerText = (`${num} is a even number`);
      } else{
          answer.innerText = (`${num} is an odd number`);
      }
  }else{
      answer.innerText = ('Please enter a positive integer');
  }
}

function hasPassed(){
  const subjects = ['Maths', 'Science', 'English', 'Marathi', 'Hindi'];
  var score = 0;
  var total = 0;

  for(let i = 0; i < subjects.length; i++){
    var user_input = parseInt(prompt(`Enter marks for ${subjects[i]}`));
    score += user_input;
    total += 100;
  }

  var percent = (score/total) * 100;

  if(percent > 30){
      answer.innerText = `Score: ${score}\nPercentage: ${percent}%\nStudent has passed`;
  }else{
      answer.innerText = `Score: ${score}\nPercentage: ${percent}%\nStudent has failed`;
  }
}

function isEquilateral(){
  const str = input.value.split(',');
  const a1 = parseInt(str[0]), a2 = parseInt(str[1]), a3 = parseInt(str[2]);

  if(a1 == 60 && a2 == 60 && a3 == 60){
    answer.innerText = ('An equilateral triangle can be formed with the given input');
  }else{
      answer.innerText = ('An equilateral triangle cannot be formed with the given input');
  }
}

function number_guessing_game(){
  var num = Math.floor(Math.random() * 100);

  for(let i = 0; i<10; i++){
      var user_input = parseInt(prompt(`Guess a number between 1-100\nYou have ${10-i} chances left`));

      if(user_input == num){
          answer.innerText = 'You have guessed correctly\nThank you for playing';
          return;
      }

      if(num > user_input){
          alert(`${user_input} is less than the generated number`);
      }else{
          alert(`The generated number is less than ${user_input}`);
      }
  }
}

// LOOP

function OnetoNum(){
  var user_input = parseInt(input.value);

  for(let i = 1; i <= user_input; i++){
      answer.innerText += i +'\n';
  }
}

function allPrimeNum(){
  var divisibleNums = [];
  var primeNums = [];

  for(let i = 2; i <= 100; i++){
    divisibleNums = [];
    for(let j = 2; j<i; j++){
      if((i%j)==0){
        divisibleNums.push(i);
      }
    }
    if(divisibleNums.length == 0) primeNums.push(i);
  }
  answer.innerText = primeNums;
}

function armstrong(){
  var sum = 0;

  for(let i =100; i < 1000; i++){
      str = i.toString();
      n1 = parseInt(str[0]);
      n2 = parseInt(str[1]);
      n3 = parseInt(str[2]);
      sum = (n1*n1*n1)+(n2*n2*n2)+(n3*n3*n3);
      if(sum==i) answer.innerText += sum+'\n';
  }
}

function ascii(){
  for(let i = 65; i<=90; i++){
    answer.innerText += String.fromCharCode(i)+"\n";
  }
}

function commonFactors(){
  var user_input = input.value;
  const num1 = parseInt(user_input.split(' ')[0]);
  const num2 = parseInt(user_input.split(' ')[1]);
  const smallerNum = num1>num2 ? num2 : num1;
  const cf = [];
  var hcf = 0;

  for(let i = 2; i <= smallerNum; i++){
    if(num1%i==0 && num2%i==0) hcf = i;
  }

  answer.innerText = `The common factors of ${num1} and ${num2} are ${hcf}`;
}

// SWITCH

function monthToWords(){
  var num = input.value;

  switch(parseInt(num)){
      case 1: answer.innerText = ('January');
              break;
      case 2: answer.innerText = ('Febuary');
              break;
      case 3: answer.innerText = ('March');
              break;
      case 4: answer.innerText = ('April');
              break;
      case 5: answer.innerText = ('May');
              break;
      case 6: answer.innerText = ('June');
              break;
      case 7: answer.innerText = ('July');
              break;
      case 8: answer.innerText = ('September');
              break;
      case 9: answer.innerText = ('September');
              break;
      case 10: answer.innerText = ('October');
              break;
      case 11: answer.innerText = ('November');
              break;
      case 12: answer.innerText = ('December');
              break;
      default: answer.innerText = ('Please enter a valid number of month');
              break;
  }
}

function numToWords(){
  var user_input = parseInt(input.value);

  switch(user_input){
      case 0: answer.innerText = 'Zero';
              break;
      case 1: answer.innerText = 'One';
              break;
      case 2: answer.innerText = 'Two';
              break;
      case 3: answer.innerText = 'Three';
              break;
      case 4: answer.innerText = 'Four';
              break;
      case 5: answer.innerText = 'Five';
              break;
      case 6: answer.innerText = 'Six';
              break;
      case 7: answer.innerText = 'Seven';
              break;
      case 8: answer.innerText = 'Eight';
              break;
      case 9: answer.innerText = 'Nine';
              break;
      default: answer.innerText = 'Please enter a single digit number';
              break;
  }
}

function operation_from_user_input(){
  var user_input = input.value.split(' ');

  var n1 = parseInt(user_input[0]);
  var operator = user_input[1];
  var n2 = parseInt(user_input[2]);

  let result;
  switch(operator){
      case '+': result = n1 + n2;
                break;
      case '-': result = n1 - n2;
                break;
      case '*': result = n1 * n2;
                break;
      case '/': result = n1 / n2;
                break;
      case '%': result = n1 % n2;
                break;
      default: result = 'Invalid operator provided';
              break;
  }

  answer.innerText = `${n1} ${operator} ${n2} = ${result}`;
}

function totalVowels(){
  var user_input = input.value;
  var vowels = [];
  for(let i = 0; i < user_input.length; i++){
    switch(user_input[i]){
        case 'a': vowels.push(user_input[i]);
                  break;
        case 'e': vowels.push(user_input[i]);
                  break;
        case 'i': vowels.push(user_input[i]);
                  break;
        case 'o': vowels.push(user_input[i]);
                  break;
        case 'u': vowels.push(user_input[i]);
                  break;
    }
  }

  answer.innerText = `There are ${vowels.length} vowels. ${vowels}`;
}

function totalVowelsOfType(){
  var user_input = input.value.toLocaleLowerCase();
  var vowels = [[], [], [], [], []];
  answer.innerText = (vowels[0]);

  for(let i = 0; i < user_input.length; i++){
    switch(user_input[i]){
        case 'a': vowels[0].push(user_input[i]);
                  break;
        case 'e': vowels[1].push(user_input[i]);
                  break;
        case 'i': vowels[2].push(user_input[i]);
                  break;
        case 'o': vowels[3].push(user_input[i]);
                  break;
        case 'u': vowels[4].push(user_input[i]);
                  break;
    }
  }

  answer.innerText = `There are ${vowels[0].length+vowels[1].length+vowels[2].length+vowels[3].length+vowels[4].length} vowels. 
  'A' is present ${vowels[0].length} times. 'E' is present ${vowels[1].length} times. 
  'I' is present ${vowels[2].length} times. 'O' is present ${vowels[3].length} times. 'U' is present ${vowels[4].length} times.`;
}

function greeting(message, name){
  answer.innerText = `${message} ${name}!`;
}