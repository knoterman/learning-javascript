var user_input = prompt('Enter 2 numbers');
const num1 = parseInt(user_input.split(' ')[0]);
const num2 = parseInt(user_input.split(' ')[1]);
const smallerNum = num1>num2 ? num2 : num1;
const cf = [];
var hcf = 0;

for(let i = 2; i <= smallerNum; i++){
  if(num1%i==0 && num2%i==0) hcf = i;
}

alert(`The common factors of ${num1} and ${num2} are ${hcf}`);
