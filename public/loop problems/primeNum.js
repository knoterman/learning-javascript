var user_input = parseInt(prompt('Enter a number'));

let divisibleNums = [];

for(let i = 2; i < user_input-2; i++){
  if(user_input % i == 0)divisibleNums.push(i);
}

if(divisibleNums.length == 0){
  console.log(`${user_input} is a prime number`);
}else{
  console.log(`${user_input} is not a prime number. It is divisible by ${divisibleNums}`);
}
