var num = 3;

for(let i = 1; i <=10; i++){
    console.log(`${num} * ${i} = ${num * i}`);
}

// USING WHILE LOOP

// var i = 1;

// while(i<=10){
//     console.log(`${num} * ${i} = ${num * i}`);
//     i++;
// }