var user_input = prompt('Enter the operation').split(' ');

var n1 = parseInt(user_input[0]);
var operator = user_input[1];
var n2 = parseInt(user_input[2]);

let result;
switch(operator){
    case '+': result = n1 + n2;
              break;
    case '-': result = n1 - n2;
              break;
    case '*': result = n1 * n2;
              break;
    case '/': result = n1 / n2;
              break;
    case '%': result = n1 % n2;
              break;
    default: result = 'Invalid operator provided';
             break;
}

alert(`${n1} ${operator} ${n2} = ${result}`);