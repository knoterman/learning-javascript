var user_input = prompt('Enter some text').toLocaleLowerCase();
var vowels = [[], [], [], [], []];
console.log(vowels[0]);

for(let i = 0; i < user_input.length; i++){
  switch(user_input[i]){
      case 'a': vowels[0].push(user_input[i]);
                break;
      case 'e': vowels[1].push(user_input[i]);
                break;
      case 'i': vowels[2].push(user_input[i]);
                break;
      case 'o': vowels[3].push(user_input[i]);
                break;
      case 'u': vowels[4].push(user_input[i]);
                break;
  }
}

alert(`There are ${vowels[0].length+vowels[1].length+vowels[2].length+vowels[3].length+vowels[4].length} vowels. 
'A' is present ${vowels[0].length} times. 'E' is present ${vowels[1].length} times. 
'I' is present ${vowels[2].length} times. 'O' is present ${vowels[3].length} times. 'U' is present ${vowels[4].length} times.`);